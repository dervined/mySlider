// Флаг - пауза.
isPaused = false;

// Запуск слайдера (автоматическая смена слайдов каждые **мс)
startSliders = (ms) => {
    sliders = document.getElementsByClassName('dv_slider');
    sliders_arr = Array.prototype.slice.call(sliders);
    sliders_arr.forEach((slider) => {
        processingImg(slider);
    });
    setInterval(() => {
        if (!isPaused) {
            sliders_arr.forEach((slider) => {
                right(slider);
            });
        }
    }, ms);
}
// Остановка/Запуск слайдера
toogleSliders = () => {
    isPaused = !isPaused;
    console.log('isPaused: '+isPaused);
}
// Переключить слайд
right = (slider) => {
    elements = comingElements(slider);
    if (elements[0].classList.contains('swipe-left')) {
        elements[0].classList.remove('swipe-left')
    }
    elements[1].classList.add('swipe-left');
    elements[1].classList.remove('showing');
    elements[2].classList.add('showing');
}
// Подготовка изображений слайда
processingImg = (slider) => {
    slides = slider.children;
    slides_arr = Array.prototype.slice.call(slides);
    bg_img = (item, img) => {
        bg_blur = document.createElement('div');
        bg_blur.classList.add('bg_blur');
        item.appendChild(bg_blur);
        src_img = String(img.getAttribute('src'))
        bg_blur.style.backgroundImage = 'url(' + src_img + ')';
        item.style.backgroundImage = 'url(' + src_img + ')';
    }
    slides_arr.forEach((item, i) => {
        if (item.children.length != 0) {
            if (item.children[0].localName == 'img') {
                img = item.children[0];
                w1 = img.offsetWidth / img.offsetHeight;
                w2 = item.offsetWidth / item.offsetHeight;
                if (w1 < w2) {
                    img.classList.add('img_hcenter');
                    bg_img(item, img)
                }
                else if (w1 > w2) {
                    img.classList.add('img_vcenter');
                    bg_img(item, img)
                }
                else img.classList.add('img_center');
            }
        }
    });
}
// Массив ближайших элементов (предыдущий, текущий, следующий)
comingElements = (slider) => {
    slides = slider.children;
    slides_arr = Array.prototype.slice.call(slides)
    slide_showing_index = null;
    for (var i = 0; i < slides_arr.length; i++) {
        if (slides_arr[i].classList.contains('showing')) {
            slide_showing_index = i;
            break;
        }
    }

    prev_element = slides[prev_index(slide_showing_index, slides_arr.length)]
    current_element = slides[slide_showing_index]
    next_element = slides[next_index(slide_showing_index, slides_arr.length)]

    return [prev_element, current_element, next_element]
}
// Получение индекса предыдущего элемента
prev_index = (current_index, length_array) => {
    return current_index == 0 ? length_array - 1 : current_index - 1;
}
// Получение индекса следующего элемента
next_index = (current_index, length_array) => {
    return current_index == length_array - 1 ? 0 : current_index + 1;
}

